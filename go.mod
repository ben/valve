module git.lubar.me/ben/valve

go 1.17

require (
	github.com/google/gapid v1.6.1
	github.com/x448/float16 v0.8.4
	golang.org/x/text v0.5.0
	golang.org/x/tools v0.4.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/mod v0.7.0 // indirect
	golang.org/x/sys v0.3.0 // indirect
)
