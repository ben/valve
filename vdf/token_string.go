// Code generated by "stringer -type Token -trimprefix Token"; DO NOT EDIT.

package vdf

import "strconv"

func _() {
	// An "invalid array index" compiler error signifies that the constant values have changed.
	// Re-run the stringer command to generate them again.
	var x [1]struct{}
	_ = x[TokenSpace-0]
	_ = x[TokenComment-1]
	_ = x[TokenString-2]
	_ = x[TokenQuoted-3]
	_ = x[TokenCond-4]
	_ = x[TokenOpenBrace-5]
	_ = x[TokenCloseBrace-6]
}

const _Token_name = "SpaceCommentStringQuotedCondOpenBraceCloseBrace"

var _Token_index = [...]uint8{0, 5, 12, 18, 24, 28, 37, 47}

func (i Token) String() string {
	if i >= Token(len(_Token_index)-1) {
		return "Token(" + strconv.FormatInt(int64(i), 10) + ")"
	}
	return _Token_name[_Token_index[i]:_Token_index[i+1]]
}
